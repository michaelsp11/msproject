<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get('/aboutas', function () {
    return view('welcome');
});

Route::get('/contact', function () {
    return view('welcome');
});

Route::get('/blog', function () {
    return view('welcome');
});

Route::get('/post/{id}/{name}', function ($id,$name) {
    return "Este es el post nº " . $id. " creado por ".$name;
})->where('name','[A-Za-z]+');*/

//Route::get('/inicio/{id}', 'Default3Controller@index');
Route::get('/', 'PagesController@index');
Route::resource('/','DefaultController');
Route::get('/aboutas', 'PagesController@aboutas');
Route::get('/contact', 'PagesController@contact');

@extends('base')
@section('title')
Contacto
@endsection
@section('sidebar')

    @parent

@endsection

@section('content')
            <div class="row vh-100 flex align-items-center">
                    <div class="col-sm-12 col-md-7 text-center">
                            <h1>Contacto</h1>
                            <h5>Contáctame: 635382150</h5>
                    </div>
                    <div class="col-sm-12 col-md-5 text-center">
                      <form>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Nombre</span>
                          </div>
                          <input type="text" class="form-control" placeholder="Escribe tu nombre">
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Apellido</span>
                          </div>
                          <input type="text" class="form-control" placeholder="Escribe tu apellido">
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                          </div>
                          <input type="text" class="form-control" placeholder="Email">
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-phone"></i></span>
                          </div>
                          <input type="text" class="form-control" placeholder="Teléfono">
                        </div>
                        <input id="subcontact" class="btn btn-primary" type="submit" value="Enviar consulta">
                        <!--<div class="input-group mb-3">
                          <input type="text" class="form-control" placeholder="Your Email">
                          <div class="input-group-append">
                            <span class="input-group-text">@example.com</span>
                          </div>
                        </div>-->
                      </form> 
                    </div>
            </div>
@endsection

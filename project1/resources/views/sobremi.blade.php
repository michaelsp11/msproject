@extends('base')
@section('title')
Sobre Mi
@endsection
@section('sidebar')

    @parent

@endsection

@section('content')

            <div class="row vh-100 flex align-items-center">
                    <div class="col-12 text-center">
                            <h1>Sobre Mi</h1>
                            <h5>Programador junior y futuro full-stack developer.</h5>
                    </div>
                        
                </div>
                
            </div>
@endsection

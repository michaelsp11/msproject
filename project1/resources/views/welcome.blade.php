@extends('base')
@section('title')
Inicio
@endsection
@section('sidebar')

    @parent

@endsection

@section('content')

                <div class="row vh-100 flex align-items-center">
                    <div class="col-12 text-center">
                        <h1>Primera página de Michael</h1>
                        <h5>El comienzo de una nueva aventura...</h5>
                    </div>
                </div>

@endsection

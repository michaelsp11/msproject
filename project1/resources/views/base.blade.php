<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Michael - @yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }
            .text-gray{
                color:gray;
            }
            .xarxes li {
                list-style-type: none;
            }

            .xarxes li a {
                width: 60px;
                height: 60px;
                background: #fff;
                text-align: center;
                line-height: 55px;
                font-size: 35px;
                margin: 0 10px;
                display: block;
                border-radius: 50%;
                position: relative;
                overflow: hidden;
                border: 3px solid #fff;
                z-index: 2;
            }

            .xarxes li a .fa {
                position: relative;
                color: #262626;
                transition: 0.5s;
                z-index: 3;
            }

            .xarxes li a:hover .fa {
                color: #fff;
                transform: rotateY(360deg);
            }

            .xarxes li a:before {
                content: '';
                position: absolute;
                top: 100%;
                left: 0;
                width: 100%;
                height: 100%;
                background: #f00;
                transition: 0.5s;
                z-index: 2;
            }

            .xarxes li a:hover:before {
                top: 0;
            }

            .xarxes li:nth-child(1) a:before {
                background: #3b5999;
            }

            .xarxes li:nth-child(2) a:before {
                background: #0073b3;
            }

            .xarxes li:nth-child(3) a:before {
                background: #dd4b39;
            }

        </style>
    </head>
    <body>
    
        @section('sidebar')
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-center">
                <a class="navbar-brand" href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                            <a class="nav-link text-gray" href="{{ url('/') }}">Inicio</a>
                        </li>
                        <li class="nav-item {{ Request::is('aboutas') ? 'active' : '' }}">
                            <a class="nav-link text-gray" href="{{ url('/aboutas') }}">Sobre Mi</a>
                        </li>
                    <!--<li class="{{ Request::is('auth/login') ? 'active' : '' }}">-->
                        <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}">
                            <a class="nav-link text-gray" href="{{ url('/contact') }}">Contacto</a>
                        </li>
                        
                    </ul>

                    @if (Route::has('login'))
                        <div class="top-right links">
                        @auth
                            <a href="{{ url('/') }}">Home</a>
                        @else
                            <a href="{{ route('login') }}">Login</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">Register</a>
                            @endif
                        @endauth
                        </div>
                    @endif
                </div>
            </nav>
        </header>
        @show
        <main>
            <div class="container">
                @yield('content')
            </div>
        </main>
        <footer class="bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="text-white mt-4">Redes sociales:</h3>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                        <ul class="list-unstyled list-inline social text-center xarxes d-flex justify-content-center">
                            <li><a href="https://www.facebook.com/michael.salper" class="w-20 h-20" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/michael-salgado-a20929187/" class="w-20 h-20" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="mailto:michael11041941@hotmail.com" class="w-20 h-20"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                        <p class="h6">&copy All right Reversed. Michael Salgado Perez</p>
                    </div>

                </div>
            </div>
        </footer>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>